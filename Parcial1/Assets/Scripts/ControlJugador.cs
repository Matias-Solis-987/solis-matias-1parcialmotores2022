﻿using UnityEngine;
public class ControlJugador : MonoBehaviour 
{ 
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;
    public LayerMask capaPiso; 
    public float magnitudSalto; 
    public CapsuleCollider col;
    private Rigidbody rb;
    void Start() 
    { 
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>(); 
        col = GetComponent<CapsuleCollider>();
    }
    void Update() 
    { 
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento; 
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento; 
        movimientoAdelanteAtras *= Time.deltaTime; 
        movimientoCostados *= Time.deltaTime; 
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (Input.GetKeyDown("escape")) 
        { 
            Cursor.lockState = CursorLockMode.None; 
        }
        if (Input.GetMouseButtonDown(0)) 
        { 
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); 
            RaycastHit hit; 
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5) 
            { 
                Debug.Log("El rayo tocó al objeto: " + hit.collider.name); 
            } 
        }
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
}
